package auth

import (
	"net/http"
	"fmt"
	//"log"

	//"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	//"github.com/markbates/goth/providers/google"
)

type Provider string

const (
	ProviderGoogle Provider = "Google"
	ProviderFacebook Provider = "Facebook"
)

type Config struct {
	Providers []ProviderInfo
}

type ProviderInfo struct {
	Provider Provider
	Creds Credentials
}

type Credentials struct {
	Key string
	Secret string
}

//Need to access Session Store in the Init somehow
//Rework to take in c Config and provision all given providers
func Init(c Config){
	//goth.UseProviders(google.New(d.Creds.Key, d.Creds.Secret, "http://localhost:8000/auth/google/callback"))
}



func AuthInitiateHandler(req *http.Request, res http.ResponseWriter){
	user, err := gothic.CompleteUserAuth(res, req)
	if err != nil {
		fmt.Fprintln(res, err)
		return
	}
	fmt.Println(user)
}

func AuthCallbackHandler(req *http.Request, res http.ResponseWriter){
	gothic.BeginAuthHandler(res, req)
}